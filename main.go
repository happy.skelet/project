package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"testProject/internal/handlers"
	"testProject/utils/config"
	"testProject/utils/kafka"
)

// @title Fact Buffer API
// @version 1.0
// @description API для буферизации и сохранения фактов с использованием Kafka.
// @termsOfService http://swagger.io/terms/

// @contact.name Кан Дэ Хан
// @contact.url https://t.me/happyskelet

// @host localhost:8585
// @BasePath /
func main() {
	cfg, err := config.LoadConfig()
	if err != nil {
		log.Fatalf("Error loading config: %v", err)
	}

	producer, err := kafka.NewKafkaProducer(strings.Split(cfg.KafkaBrokers, ","))
	if err != nil {
		log.Fatalf("Failed to create Kafka producer: %v", err)
	}
	defer producer.Close()

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt, syscall.SIGTERM)

	ctx, cancel := context.WithCancel(context.Background())

	go handlers.RunServer(cfg, producer)
	go kafka.StartConsumer(ctx, cfg, producer)

	<-signals
	cancel()
	log.Println("Shutting down")
}
