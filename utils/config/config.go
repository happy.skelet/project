package config

import (
	env "github.com/caarlos0/env/v6"
)

// Config содержит все переменные окружения для конфигурации приложения
type Config struct {
	KafkaBrokers       string `env:"KAFKA_BROKERS" envDefault:"localhost:9092"`
	KafkaFactTopic     string `env:"KAFKA_FACT_TOPIC" envDefault:"fact_buffer"`
	KafkaResponseTopic string `env:"KAFKA_RESPONSE_TOPIC" envDefault:"fact_response"`
	SaveFactEndpoint   string `env:"SAVE_FACT_ENDPOINT"`
	AuthToken          string `env:"AUTH_TOKEN"`
	ServerPort         string `env:"SERVER_PORT" envDefault:"8585"`
	SwaggerHost        string `env:"SWAGGER_HOST" envDefault:"localhost:8585"`
}

// LoadConfig загружает конфигурацию из .env файла
func LoadConfig() (*Config, error) {
	cfg := Config{}
	if err := env.Parse(&cfg); err != nil {
		return nil, err
	}
	return &cfg, nil
}
