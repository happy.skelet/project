package kafka

import (
	"context"
	"encoding/json"
	"github.com/Shopify/sarama"
	"log"
	"strings"
	"testProject/internal/models"
	"testProject/internal/rpc"
	"testProject/utils/config"
)

// FactConsumerGroupHandler обработчик сообщений из Kafka
type FactConsumerGroupHandler struct {
	Producer sarama.SyncProducer
	Config   *config.Config
}

// NewKafkaProducer создает новый продюсер Kafka
func NewKafkaProducer(brokers []string) (sarama.SyncProducer, error) {
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = 5
	config.Producer.Return.Successes = true

	producer, err := sarama.NewSyncProducer(brokers, config)
	if err != nil {
		return nil, err
	}
	return producer, nil
}

// NewKafkaConsumerGroup создает новую группу консюмера Kafka
func NewKafkaConsumerGroup(brokers []string, groupID string) (sarama.ConsumerGroup, error) {
	config := sarama.NewConfig()
	config.Version = sarama.V2_1_0_0
	config.Consumer.Return.Errors = true

	consumerGroup, err := sarama.NewConsumerGroup(brokers, groupID, config)
	if err != nil {
		return nil, err
	}

	return consumerGroup, nil
}

// Setup вызывается перед началом обработки сообщений
func (h *FactConsumerGroupHandler) Setup(sarama.ConsumerGroupSession) error {
	return nil
}

// Cleanup вызывается после завершения обработки сообщений
func (h *FactConsumerGroupHandler) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

// ConsumeClaim обрабатывает сообщения из Kafka
func (h *FactConsumerGroupHandler) ConsumeClaim(sess sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for msg := range claim.Messages() {
		var fact models.Fact
		if err := json.Unmarshal(msg.Value, &fact); err != nil {
			log.Printf("Failed to unmarshal message: %v", err)
			continue
		}

		if err := rpc.SaveFactAndRespond(fact, h.Config, h.Producer); err != nil {
			log.Printf("Failed to save fact and respond: %v", err)
		}

		sess.MarkMessage(msg, "")
	}
	return nil
}

// StartConsumer запуск консьюмера
func StartConsumer(ctx context.Context, cfg *config.Config, producer sarama.SyncProducer) {
	consumerGroup, err := NewKafkaConsumerGroup(strings.Split(cfg.KafkaBrokers, ","), "fact_consumer_group")
	if err != nil {
		log.Fatalf("Failed to create Kafka consumer group: %v", err)
	}
	defer consumerGroup.Close()

	for {
		select {
		case <-ctx.Done():
			log.Println("Consumer stopped")
			return
		default:
			if err := consumerGroup.Consume(ctx, []string{cfg.KafkaFactTopic}, &FactConsumerGroupHandler{Producer: producer, Config: cfg}); err != nil {
				log.Printf("Error from consumer: %v", err)
			}
		}
	}
}

// SendToKafka отправляет сообщение в топик Kafka
func SendToKafka(producer sarama.SyncProducer, fact models.Fact, cfg *config.Config) error {
	jsonData, err := json.Marshal(fact)
	if err != nil {
		return err
	}

	message := &sarama.ProducerMessage{
		Topic: cfg.KafkaFactTopic,
		Value: sarama.StringEncoder(jsonData),
	}

	_, _, err = producer.SendMessage(message)
	if err != nil {
		return err
	}

	return nil
}
