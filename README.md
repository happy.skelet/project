# TestProject

## Описание

Буфер для поступающих фактов который сохраняет записи в kafka для ассинхронной отправки большого объема данных
После запроса сохраняет в другой топик ответ от сервера


## Get started
Я добавил docker-compose файл для red-panda и red-panda console для наглядного просмотра топиков 
для установки контейнера в докере

docker compose up -d

После запуска в докере red-panda-console доступна по ссылке:
http://localhost:8080/topics

Для запуска проекта:

go mod download
go run main.go

при необходимости изменить .env файл 

Swagger доступен после запуска по ссылке:
http://localhost:8585/swagger/index.html