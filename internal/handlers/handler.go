package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/Shopify/sarama"
	"github.com/google/uuid"
	httpSwagger "github.com/swaggo/http-swagger"
	"log"
	"net/http"
	_ "testProject/docs"
	"testProject/internal/models"
	"testProject/utils/config"
	"testProject/utils/kafka"
)

// @Summary Создание фактов
// @Description Создает факты согласно переданным данным в формате JSON
// @ID create-facts
// @Accept json
// @Produce json
// @Param facts body []models.Fact true "Факты"
// @Success 200 {string} string "OK"
// @Failure 400 {string} string "Failed to parse request body"
// @Failure 500 {string} string "Failed to send message to Kafka"
// @Router /facts [post]
func createFactsHandler(producer sarama.SyncProducer, cfg *config.Config) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var facts []models.Fact
		if err := json.NewDecoder(r.Body).Decode(&facts); err != nil {
			http.Error(w, "Failed to parse request body", http.StatusBadRequest)
			return
		}

		for _, fact := range facts {
			fact.RequestId = uuid.New().String()
			if err := kafka.SendToKafka(producer, fact, cfg); err != nil {
				log.Printf("Failed to send message to Kafka: %v", err)
				http.Error(w, "Failed to send message to Kafka", http.StatusInternalServerError)
				return
			}
		}

		w.WriteHeader(http.StatusOK)
		fmt.Fprintln(w, "Факты успешно добавленны в буфер")
	}
}

// RunServer запускает HTTP-сервер
func RunServer(cfg *config.Config, producer sarama.SyncProducer) {
	http.HandleFunc("/facts", createFactsHandler(producer, cfg))
	http.HandleFunc("/swagger/", httpSwagger.WrapHandler)
	log.Printf("HTTP server started on :%s", cfg.ServerPort)
	if err := http.ListenAndServe(":"+cfg.ServerPort, nil); err != nil {
		log.Fatalf("HTTP server error: %v", err)
	}
}
