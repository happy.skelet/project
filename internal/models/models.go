package models

type Fact struct {
	RequestId           string `json:"request_id"`
	PeriodStart         string `json:"period_start" example:"2024-05-01"`
	PeriodEnd           string `json:"period_end" example:"2024-05-31"`
	PeriodKey           string `json:"period_key" example:"month"`
	IndicatorToMoID     string `json:"indicator_to_mo_id" example:"227373"`
	IndicatorToMoFactID string `json:"indicator_to_mo_fact_id" example:"0"`
	Value               string `json:"value" example:"1"`
	FactTime            string `json:"fact_time" example:"2024-05-31"`
	IsPlan              string `json:"is_plan" example:"0"`
	AuthUserID          string `json:"auth_user_id" example:"40"`
	Comment             string `json:"comment" example:"buffer Last_name"`
}

type FactResponse struct {
	RequestId string   `json:"request_id"`
	Messages  Messages `json:"MESSAGES"`
	Data      Data     `json:"DATA"`
	Status    string   `json:"STATUS"`
}

type Messages struct {
	Error   []string `json:"error"`
	Warning []string `json:"warning"`
	Info    []string `json:"info"`
}

type Data struct {
	IndicatorToMoFactID int `json:"indicator_to_mo_fact_id"`
}
