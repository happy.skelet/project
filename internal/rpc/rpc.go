package rpc

import (
	"encoding/json"
	"fmt"
	"github.com/Shopify/sarama"
	"log"
	"net/http"
	"net/url"
	"strings"
	"testProject/internal/models"
	"testProject/utils/config"
	"time"
)

// SaveFactAndRespond отправляет POST-запрос с фактом и записывает ответ в fact_response
func SaveFactAndRespond(fact models.Fact, cfg *config.Config, producer sarama.SyncProducer) error {
	data := url.Values{}
	data.Set("period_start", fact.PeriodStart)
	data.Set("period_end", fact.PeriodEnd)
	data.Set("period_key", fact.PeriodKey)
	data.Set("indicator_to_mo_id", fact.IndicatorToMoID)
	data.Set("indicator_to_mo_fact_id", fact.IndicatorToMoFactID)
	data.Set("value", fact.Value)
	data.Set("fact_time", fact.FactTime)
	data.Set("is_plan", fact.IsPlan)
	data.Set("auth_user_id", fact.AuthUserID)
	data.Set("comment", fact.Comment)

	req, err := http.NewRequest("POST", cfg.SaveFactEndpoint, strings.NewReader(data.Encode()))
	if err != nil {
		return fmt.Errorf("failed to create request: %v", err)
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Authorization", "Bearer "+cfg.AuthToken)

	client := &http.Client{Timeout: 10 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("Request %s \nError: failed to send request: %v", fact.RequestId, err)
	}
	defer resp.Body.Close()

	var factResponse models.FactResponse
	if err = json.NewDecoder(resp.Body).Decode(&factResponse); err != nil {
		return fmt.Errorf("Request %s \nError: failed to decode response: %v", fact.RequestId, err)
	}
	factResponse.RequestId = fact.RequestId

	jsonData, err := json.Marshal(factResponse)
	if err != nil {
		return fmt.Errorf("Request %s \nError: failed to marshal response: %v", fact.RequestId, err)
	}

	responseMessage := &sarama.ProducerMessage{
		Topic: cfg.KafkaResponseTopic,
		Value: sarama.StringEncoder(jsonData),
	}

	_, _, err = producer.SendMessage(responseMessage)
	if err != nil {
		return fmt.Errorf("Request %s \nError: failed to send response message to Kafka: %v", fact.RequestId, err)
	}

	log.Printf("Sent response to Kafka: %v", factResponse)
	return nil
}
